import Vue from 'vue'
import App from './App.vue'
import router from './router'
import BootstrapVue from "bootstrap-vue";
import VueIziToast from 'vue-izitoast';
import VueUploadComponent from "vue-upload-component";
import i18n from './i18n';

import 'izitoast/dist/css/iziToast.min.css';
import "bootstrap/dist/css/bootstrap.css"
import "bootstrap-vue/dist/bootstrap-vue.css"
import "bootstrap-dark/src/bootstrap-dark.css";

Vue.component('file-upload', VueUploadComponent)
Vue.use(BootstrapVue)
Vue.use(VueIziToast, {
  progressBar: false,
  closeOnClick: true,
  icon: false
});

Vue.config.productionTip = false

new Vue({
  router,
  i18n,
  render: h => h(App)
}).$mount('#app')
