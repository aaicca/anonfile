import firebase from "firebase";

const firebaseConfig = {
    apiKey: "AIzaSyDiyzfhSkr8WfkVflpShS0CcMwIkTkh_Jg",
    authDomain: "anonfile-9a91e.firebaseapp.com",
    databaseURL: "https://anonfile-9a91e.firebaseio.com",
    projectId: "anonfile-9a91e",
    storageBucket: "anonfile-9a91e.appspot.com",
    messagingSenderId: "725929905227",
    appId: "1:725929905227:web:c4f70947c0a3a35c4d51ce",
    measurementId: "G-H3EKP8VE8F"
  };
  
firebase.initializeApp(firebaseConfig);

const db = firebase.firestore();

const filesCol = db.collection("file");
const feedsCol = db.collection("feed");
const storage = firebase.storage();
const auth = firebase.auth();
db.settings({});

export {
    storage,
    db,
    auth,
    filesCol,
    feedsCol,
    firebase
}